package edu.miami.sorting;

import edu.miami.sorting.GnomeSort;
import edu.miami.sorting.PancakeSort;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import java.util.Random;
import java.util.Arrays;

/**
 * Tests for my gnome sorting algorithm with random numbers
 */
public class GnomeTest 
{
    @Test
    public void sortArrays() {
        int[] sizes = {10, 100, 1000, 10000};
        for (int i : sizes) {
            assertTrue(sortAndTest(i));
        }

    }

    /**
     * Test sorting algorithm with pseudorandom array. Use built-in for comparison
     * @param size size of pseudorandom array
     * @return true if test succeeded false otherwise
     */
    public boolean sortAndTest(int size) {
        Random rand = new Random();
        int[] arr = new int[size]; 
        for (int i = 0; i < size; i++) {
           if (rand.nextBoolean()) {
               arr[i] = rand.nextInt();
           } else {
               arr[i] = -1*rand.nextInt();
           }
        }
        int[] sortedCopy = Arrays.copyOf(arr, size);
        Timer t1 = new Timer();
        Arrays.sort(sortedCopy);
        Timer t = new Timer();
        GnomeSort.sort(arr);
        System.out.format("Time for sorting array of size %d: %d ms\n", size, t.getTime());
        return Arrays.equals(sortedCopy, arr);

    }
}