package edu.miami.sorting;

import org.junit.Test;

import java.util.Arrays;
import java.util.Random;

import static org.junit.Assert.assertTrue;

/**
 * Unit tests for GnomeArmy sorting
 */
public class GnomeArmyTest
{
    @Test
    public void sortArrays() {
        int[] sizes = {10, 100, 1000, 10000, 100000};
        for (int i : sizes) {
            try {
                assertTrue(sortAndTest(i));
            } catch(InterruptedException e) {
                System.out.println("Interrupted");
            }
        }

    }

    /**
     * Test sorting algorithm with pseudorandom array. Use built-in for comparison
     * @param size size of pseudorandom array
     * @return true if test succeeded false otherwise
     * @throws InterruptedException if threads interrupted
     */
    public boolean sortAndTest(int size) throws InterruptedException {
        Random rand = new Random();
        int[] arr = new int[size]; 
        for (int i = 0; i < size; i++) {
           if (rand.nextBoolean()) {
               arr[i] = rand.nextInt();
           } else {
               arr[i] = -1*rand.nextInt();
           }
        }
        int[] sortedCopy = Arrays.copyOf(arr, size);
        Timer t1 = new Timer();
        Arrays.sort(sortedCopy);
        System.out.format("Java sorting alone for array of size %d took %d ms\n", size, t1.getTime());
        Timer t = new Timer();
        GnomeArmySort ga = new GnomeArmySort(arr, 13);
        System.out.format("Sorting for array of size %d took %d ms\n", size, t.getTime());
        arr = ga.result();
        return Arrays.equals(sortedCopy, arr);

    }
}