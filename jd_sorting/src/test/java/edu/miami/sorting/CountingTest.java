
package edu.miami.sorting;

import org.junit.Test;

import java.util.Arrays;
import java.util.Random;

import static org.junit.Assert.assertTrue;

/**
 * Test my counting sort on arrays with values in the range (0, 10)
 */
public class CountingTest
{
    @Test
    public void sortArrays() {
        int[] sizes = {1, 100, 1000, 10000, 100000};
        for (int i : sizes) {
            assertTrue(sortAndTest(i));
        }

    }

    /**
     * Test sorting algorithm with pseudorandom array. Use built-in for comparison
     * @param size size of pseudorandom array
     * @return true if test succeeded false otherwise
     */
    public boolean sortAndTest(int size) {
        Random rand = new Random(543543);
        int[] arr = new int[size]; 
        for (int i = 0; i < size; i++) {
           arr[i] = Math.abs(rand.nextInt(10));
        }
        int[] sortedCopy = Arrays.copyOf(arr, size);
        Arrays.sort(sortedCopy);
        Timer t = new Timer();
        CountingSort.sort(arr, 10);
        System.out.format("Time for sorting array of size %d: %d ms\n", size, t.getTime());
        return Arrays.equals(sortedCopy, arr);

    }
}