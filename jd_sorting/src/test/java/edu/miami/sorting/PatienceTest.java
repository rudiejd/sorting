
package edu.miami.sorting;

import static org.junit.Assert.assertTrue;
import org.junit.Test;
import edu.miami.sorting.PatienceSort;
import edu.miami.sorting.Timer;

import java.util.Arrays;
import java.util.Random;
/**
 * Test my patience sorting algorithm with pseudorandom arrays of increasing size
 */
public class PatienceTest 
{
    @Test
    public void sortArrays() {
        int[] sizes = {1, 10, 100, 1000, 10000, 100000};
        for (int i : sizes) {
            assertTrue(sortAndTest(i));
        }

    }

    /**
     * Test sorting algorithm with pseudorandom array. Use built-in for comparison
     * @param size size of pseudorandom array
     * @return true if test succeeded false otherwise
     */
    public boolean sortAndTest(int size) {
        Random rand = new Random();
        int[] arr = new int[size]; 
        for (int i = 0; i < size; i++) {
           if (rand.nextBoolean()) {
               arr[i] = rand.nextInt();
           } else {
               arr[i] = -1*rand.nextInt();
           }
        }
        int[] sortedCopy = Arrays.copyOf(arr, size);
        Arrays.sort(sortedCopy);
        Timer t = new Timer();
        arr = PatienceSort.sort(arr);
        System.out.format("Time for sorting array of size %d: %d ms\n", size, t.getTime());
        return Arrays.equals(arr, sortedCopy);

    }
}