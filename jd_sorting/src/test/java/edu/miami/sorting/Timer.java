package edu.miami.sorting;

import java.util.Date;

public class Timer {
    private long startTime;

    Timer() {
        startTime = System.currentTimeMillis();
    }

    public long getTime() {
        return (new Date()).getTime() - startTime;
    }


}
