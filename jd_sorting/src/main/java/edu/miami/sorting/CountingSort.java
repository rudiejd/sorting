package edu.miami.sorting;

import java.util.Arrays;
import java.util.LinkedList;

/**
 * Sort a given array using counting sort.
 */
class CountingSort {
    /**
     * Sort the input array arr of items within range [0, k]
     * @param arr input array of data to sort
     * @param k upper bound on range of possible values
     */
    public static void sort(int[] arr, int k) {
        int[] output = new int[arr.length];
        int[] count = new int[k+1];

        for (int i : arr) {
           count[i]++;
        }

        for (int i = 1; i <= k; i++) {
            count[i] += count[i-1];
        }

        for (int i = arr.length-1; i >= 0; i--) {
            output[count[arr[i]]-1] = arr[i];
            count[arr[i]]--;
        }

        for (int i = 0; i < arr.length; i++) {
            arr[i] = output[i];
        }
    }

}