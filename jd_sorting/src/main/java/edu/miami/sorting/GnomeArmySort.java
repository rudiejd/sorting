package edu.miami.sorting;


import java.util.Arrays;
import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 * Uses an army of gnomes to pre-order an array before a traditional sort
 */
class GnomeArmySort {
    int[] arr;
    int[] res;

    /**
     * Thread that runs gnome sort
     */
    class Gnome implements Runnable {
        int start;
        int stop;

        /**
         * Main constructor for a gnome threat
         * @param start index where thread's sort begins
         * @param stop index where thread's sort ends
         */
        public Gnome(int start, int stop) {
            this.start = start;
            this.stop = stop;

        }

        @Override
        public void run() {
            int i = start;
            while (i < stop) {
                if (i == start || arr[i] >= arr[i]) {
                    i++;
                } else {
                    int temp = arr[i];
                    arr[i] = arr[i-1];
                    arr[i-1] = temp;
                    i--;
                }
            }


        }
    }

    /**
     * Pre-order array using number of gnome threads then sort with traditional algorithm
     * @param arr array to sort
     * @param gnomeCount number of gnome threads to use in sorting
     * @throws InterruptedException if concurrency error occurs
     */
    public GnomeArmySort(int[] arr, int gnomeCount) throws InterruptedException {
        this.arr = arr;
        res = new int[arr.length];
        int gnomeTerritory = arr.length/gnomeCount;
        Thread[] thrs = new Thread[gnomeCount];
        for (int i = 0; i < gnomeCount; i++) {
            thrs[i] = new Thread(new Gnome(gnomeTerritory * i, gnomeTerritory * (i + 1) - 1));
            thrs[i].start();
        }


        for (int i = 0; i < thrs.length; i++) {
            thrs[i].join();
        }


        Arrays.sort(this.arr);

    }

    /**
     * Return result of sorting
     * @return sorted array
     */
    public int[] result() {
        return arr;
    }



}