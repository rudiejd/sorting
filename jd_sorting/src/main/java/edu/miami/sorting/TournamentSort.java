package edu.miami.sorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

/**
 * Basic class for implementing the tournament tree. Has
 * right and left pointers and a simple constructor.
 */
class Node {
    public int value;
    public Node left = null;
    public Node right = null;
    public Node(int value) {
        this.value = value;
    }
}


/**
 * Sort array by having elements play matches like in a tournament
 */
class TournamentSort {
    int size;
    int[] arr;
    LinkedList<Node> trees;

    /**
     * Main constructor
     * @param arr array to sort
     */
    public TournamentSort(int[] arr) {
        this.arr = arr;
        trees = new LinkedList<>();
        this.size = arr.length;
        buildForest();
    }

    /**
     * Build a tournament forest by constructing nodes with each array element
     */
    public void buildForest() {
        for (int i = 0; i < size; i++) {
            Node tree = new Node(arr[i]);
            trees.add(tree);
        }

    }

    /**
     * Equivalent to GRAB! method in original tournament sort paper.
     * This is when one tree wins a match
     * @param x winner
     * @param y loser
     * @return winner tree (two trees are combined)
     */
    public Node grab(Node x, Node y) {
        if (x.left == null) {
            x.left = y;
        } else if (x.right == null) {
            x.right = y;
        } else if (x.left != null) {
            if (x.left.value < y.value) {
                x.left = grab(x.left, y);
            } else {
                x.left = grab(y, x.left);
            }
        } else if (x.right != null && x.right.value < x.value){
            if (x.right.value < y.value) {
                x.right = grab(x.right, y);
            } else {
                x.right = grab(y, x.right);
            }
        }

        return x;

    }

    /**
     * Play match between two trees
     * @param x first tree
     * @param y second tree
     * @return winning tree (combination of the two)
     */
    public Node tournamentPlay(Node x, Node y) {
        if (x.value < y.value) {
            return grab(x, y);
        } else {
            return grab(y, x);
        }
    }

    /**
     * Play a round of the tournament. In each round, the smallest element wins.
     * Equivalent to TOURNAMENT-PLAY! in original paper.
     * @param soFar trees that have played already
     * @param toBeDone trees that haven't played already
     */
    public void tournamentRound(LinkedList<Node> soFar, LinkedList<Node> toBeDone) {

        while(!toBeDone.isEmpty()) {
            if (toBeDone.size() == 1) {
                soFar.add(toBeDone.getFirst());
                break;
            }
            Node newNode = tournamentPlay(toBeDone.get(0), toBeDone.get(1));
            soFar.addFirst(newNode);
            if (toBeDone.size() < 2) {
                toBeDone.clear();
            } else {
                toBeDone.removeFirst();
                toBeDone.removeFirst();
            }
        }
        trees = soFar;
    }

    /**
     * Continue to play tournament round until a winner is found
     * @return winning tree
     */
    public Node tournament() {
        if (trees.size() == 1) {
            return trees.get(0);
        } else {
            tournamentRound(new LinkedList<Node>(), trees);
            return tournament();
        }
    }

    /**
     * Sorting algorithm that plays tournaments to determine order of array
     * @return sorted array
     */
    public int[] sort() {
        int[] ret = new int[size];
        for (int i = 0; i < size; i++) {
            Node winner = tournament();
            ret[i] = winner.value;
            trees.clear();
            if (winner.left != null) trees.add(winner.left);

            if (winner.right != null) trees.add(winner.right);
        }
        return ret;


    }

}