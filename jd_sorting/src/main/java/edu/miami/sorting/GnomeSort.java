package edu.miami.sorting;

/**
 * Sorts an array like a gnome sorts garden pots
 */
class GnomeSort {
    /**
     * Main method for sorting array
     * @param arr array to be sorted
     */
    public static void sort(int[] arr) {
       int i = 0;
       while (i < arr.length) {
           if (i == 0 || arr[i] >= arr[i-1]) {
               i++;
           } else {
               int temp = arr[i];
               arr[i] = arr[i-1];
               arr[i-1] = temp;
               i--;
           }
       }
    }
}