package edu.miami.sorting;

class PancakeSort {   
    // Flip array [0, i)

    /**
     * Flip array in interval [0, i) (0 inclusive to i non-inclusive)
     * @param arr array in which flip should happen
     * @param i upper bound index
     */
    public static void flip(int[] arr, int i) {
        for (int j = 0; j < i; j++) {
            int temp = arr[j];
            arr[j] = arr[i];
            arr[i] = temp;
            i--; 
        }
    }

    /**
     * Index of maximum element below a certain upper bound
     * @param arr array in which search should happen
     * @param upper upper bound below which max is found
     * @return index of max element below upper
     */
    public static int maxIndex(int[] arr, int upper) {
        int maxIdx = 0;
        for (int i = 0; i < upper; i++) {
            if (arr[i] > arr[maxIdx]) {
                maxIdx = i;
            }
        } 
        
        return maxIdx;
    }

    /**
     * Static convenience method to ease use of class
     * @param arr array to be sorted
     */
    public static void sort(int[] arr) {
        int j = arr.length;
 
        while(j > 1) {
            int mxI = maxIndex(arr, j);
            if (mxI != j-1) {
                // flip max to beginning
                flip(arr, mxI);
                // flip max to where it should be
                flip(arr, j-1);
            }    
            j--; 
        }
        
    }
}
