package edu.miami.sorting;
import java.util.*;

/**
 * Sorts arrays like one sorts cards in the game of Patience/Solitare
 */
public class PatienceSort {
    List<Stack<Integer>> piles;
    int[] arr;
    public int[] result;

    /**
     * Main constructor. Takes array in and creates piles, then merges them
     * @param arr array to sort
     */
    public PatienceSort(int[] arr) {
        this.arr = arr;
        piles = new ArrayList<Stack<Integer>>();
        makePiles();
        mergePiles();
    }

    /**
     *  Helper method for merging piles. Uses a priority queue keyed on the top element of a
     *  pile to merge each sorted pile into a final sorted result.
     */
    void mergePiles() {
        PriorityQueue<Stack<Integer>> pq = new PriorityQueue<>(piles.size(), new Comparator<Stack<Integer>> () {
            @Override
            public int compare(Stack<Integer> s1, Stack<Integer> s2) {
                return s1.peek().compareTo(s2.peek());
            }

        });
        for (Stack<Integer> s : piles) {
            pq.offer(s);
        }
        result = new int[arr.length];
        int j = 0;
        while (!pq.isEmpty()) {
            Stack<Integer> smallest = pq.poll();
            int addition = smallest.pop();
            result[j] = addition;
            j++;
            if (!smallest.empty())
                pq.offer(smallest);
        }

    }

    /**
     * Make piles of cards. First card forms the first pile, then after that a card
     * is assigned to the leftmost pile with a greater top value. If no such card exists,
     * the card is assigned to a new pile. Uses binary search for better complexity.
     */
    public void makePiles() {
        for (int i = 0; i < arr.length; i++) {
            int pileIdx = searchPiles(arr[i]);
            if (pileIdx < piles.size()) {
                piles.get(pileIdx).push(arr[i]);
            } else {
                Stack<Integer> newPile = new Stack<>();
                newPile.add(arr[i]);
                piles.add(newPile);
            }
        }
    }

    /**
     * Custom binary search method that returns index of pile where item should go.
     * Index returned can be greater than size of existing piles.
     * @param value Value to find a pile for
     * @return index of pile where value should go
     */
    public int searchPiles(int value) {
        if (piles.size() == 0) return 0;
        int l = 0;
        int r = piles.size()-1;
        int mid = 0;

        while (true) {
            mid = (l + r) / 2;
            if (piles.get(mid).peek() == value) {
                return mid;
            } else if (piles.get(mid).peek() < value) {
                l = mid + 1;
                if (l > r) {
                    return mid + 1;
                }
            } else {
                r = mid - 1;
                if (l > r) {
                    return mid;
                }
            }
        }
    }


    /**
     * Static helper method to ease use of class
     * @param arr array to sort
     * @return sorted array
     */
    public static int[] sort(int[] arr) {
        PatienceSort s = new PatienceSort(arr);
        return s.result;
    }


    
}
