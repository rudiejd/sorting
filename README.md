# JD's Sorting Project

This is my submission for my final project in CSE 374 with Dr. Vaskar Raychoudhury. The project includes the following sorting algorithms: 
* Gnome sort
* Counting sort
* Pancake sort
* Tournament sort
* Patience sort

While the code is far from optimal, it does run, and it can sort arrays up to 100000 elements in a reasonable amount of time. Be advised, however, that due to the recursive nature of some of these algorithms and the lack of optimization in this project, sorting arrays with more than 1000000 elements may lead to **StackOverflow** errors. 

# Setup

Setting up this project is simple. First, make sure that you have Maven and JUnit 4 installed. Next, clone the project. You're pretty much done. 
## Testing
To test the code,  navigate to the *jd_sorting* directory and run `mvn test`. This will run the tests for all algorithms, and also provide running time statistics for each algorithm. Be aware that the `GnomeArmySort` algorithm does spawn 13 threads, so this may be rough for your CPU. 
